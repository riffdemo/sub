import pika, sys
from time import sleep

def main():
    channel = None
    while True:
        try:
            with open("ip.txt") as f:
                ip = f.readlines()[0]
            connection = pika.BlockingConnection(
                pika.ConnectionParameters(host=ip))
            channel = connection.channel()
            if (channel is not None):
                break
        except pika.exceptions.AMQPConnectionError:
            sleep(1)

    channel.exchange_declare(exchange='logs', exchange_type='fanout')

    result = channel.queue_declare(queue='', exclusive=True)
    queue_name = result.method.queue

    channel.queue_bind(exchange='logs', queue=queue_name)

    def callback(ch, method, properties, body):
        print(f" [x] {body.decode()}")

    print(' [*] Waiting for logs. To exit press CTRL+C')
    channel.basic_consume(
        queue=queue_name, on_message_callback=callback, auto_ack=True)

    channel.start_consuming()


if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        print('Interrupted')
        sys.exit(0)
        